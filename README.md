
# Manual build

Generation of build files (makefile)
- normal build
```
cmake -S . -B build
```


For debug mode
```
cmake -DCMAKE_BUILD_TYPE=Debug -S . -B build
```

For debug mode with code coverage support
```
cmake -DCODE_COVERAGE=ON -S . -B build
```


Compilation
```
cmake --build build
```


```
cd build
```

run test
```
ctest
```

run application
```
cd bin
app
```


several build results are available on https://sed-rennes.gitlabpages.inria.fr/formations/formation-ci-cd/formation-cicd-gitlab-mini-cpp-application/

