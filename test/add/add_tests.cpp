#include <gtest/gtest.h>
#include "add/add.h"

TEST(AddTest, TwoPlusTwo) {
    EXPECT_EQ(add(2, 2), 4);
}

TEST(AddTest, NegativeNumbers) {
    EXPECT_EQ(add(-1, -1), -2);
}

TEST(AddTest, LargeNumbers) {
    EXPECT_EQ(add(1000000, 1000000), 2000000);
}


